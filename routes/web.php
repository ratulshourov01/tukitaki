<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

use Illuminate\Support\Facades\App;

Route::get('/test', 'TestController@index')->middleware('test');

//for group middleware
Route::group(['middleware'=>'test'],function() {
    Route::get('/', function () {
        return view('welcome');
    });
});

Route::resource('/product', 'ProductController');

//Route::view('/message','messagePage');

Route::get('/message/{lan}',function($lan){
App::setLocale($lan);
return view('messagePage');
});

Route::resource('/pagination', 'PaginationController');

Route::get('/getUsingScpe', 'ScopeController@index');